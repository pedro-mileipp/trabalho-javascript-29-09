function produtoMatrizes(matriz1, matriz2) {
    var matriz1NumLin = matriz1.length, matriz1NumCol = matriz1[0].length,
        matriz2NumeroLinhas = matriz2.length, matriz2NumeroColunas = matriz2[0].length,
        k = new Array(matriz1NumLin);
    for (var j = 0; j < matriz1NumLin; ++j) {
        k[j] = new Array(matriz2NumeroColunas);
        for (var c = 0; c < matriz2NumeroColunas; ++c) {
            k[j][c] = 0;
            for (var i = 0; i < matriz1NumCol; ++i) {
                k[j][c] += matriz1[j][i] * matriz2[i][c];
            }
        }
    }
    return k;
}

console.log(produtoMatrizes( [ [ [2],[-1] ], [ [2],[0] ] ],[ [2,3],[-2,1] ] ));
console.log(produtoMatrizes([[4,0],[-1,-1]], [[-1,3],[2,7]] ));